package yurii.com.robocarandroid.dependencies;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import yurii.com.robocarandroid.api.APIServiceProvider;
import yurii.com.robocarandroid.api.APIServiceProviderImpl;
import yurii.com.robocarandroid.net.HttpNetRequester;
import yurii.com.robocarandroid.net.NetRequester;
import yurii.com.robocarandroid.net.PiConnector;
import yurii.com.robocarandroid.net.PiConnectorImpl;
import yurii.com.robocarandroid.net.UrlBuilder;
import yurii.com.robocarandroid.persist.PersistData;
import yurii.com.robocarandroid.persist.PersistDataImpl;
import yurii.com.robocarandroid.ui.MainActivity;
import yurii.com.robocarandroid.ui.MainActivityPresenter;
import yurii.com.robocarandroid.ui.MainActivityPresenterImpl;
import yurii.com.robocarandroid.ui.SettingDialogPresenter;
import yurii.com.robocarandroid.ui.SettingsDialog;
import yurii.com.robocarandroid.ui.SettingsDialogPresenterImpl;

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 21/12/18
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public enum DependencyRegistry {

    INSTANCE;

    /**
     * Name of the Preferences.
     */
    private static final String PREFS_NAME = "RobocarAndroidPref";

    private APIServiceProvider mApiServiceProvider;
    private PersistData mPersistData;
    private ExecutorService mExecutorService;
    private HandlerThread mMainActivityHandlerThread;
    private PiConnector mPiConnector;

    DependencyRegistry() {

    }

    public void init(@NonNull final Context context) {
        mExecutorService = Executors.newSingleThreadExecutor();
        final NetRequester requester = new HttpNetRequester();
        mApiServiceProvider = new APIServiceProviderImpl(requester);
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        mPersistData = new PersistDataImpl(sharedPreferences);
        mPiConnector = new PiConnectorImpl(mExecutorService, mPersistData);
        UrlBuilder.INSTANCE.updateBaseUrl(mPersistData.loadPiIpAddress());
    }

    public void inject(final MainActivity activity, final Bundle bundle,
                       final MainActivityPresenter.Listener listener) {
        mMainActivityHandlerThread = new HandlerThread(
                MainActivity.class.getSimpleName() + "-" + HandlerThread.class.getSimpleName()
        );
        mMainActivityHandlerThread.start();
        final Handler handler = new Handler(mMainActivityHandlerThread.getLooper());
        final MainActivityPresenter presenter = new MainActivityPresenterImpl(
                handler, mExecutorService, mApiServiceProvider, mPiConnector, listener
        );
        activity.configureWith(presenter);
    }

    public void inject(final SettingsDialog dialog) {
        final SettingDialogPresenter presenter = new SettingsDialogPresenterImpl(
                mPersistData
        );
        dialog.configureWith(presenter);
    }

    public void remove(final MainActivity activity) {
        if (mMainActivityHandlerThread == null) {
            return;
        }
        mMainActivityHandlerThread.quit();
        mMainActivityHandlerThread = null;
    }
}
