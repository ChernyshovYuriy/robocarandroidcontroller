package yurii.com.robocarandroid.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 11/29/14
 * E-Mail: chernyshov.yuriy@gmail.com
 *
 * {@link AppUtils} is a helper class which holds various help-methods
 */
public final class AppUtils {

    /**
     * Executor of the API requests.
     */
    public static final ExecutorService API_CALL_EXECUTOR = Executors.newSingleThreadExecutor();

    /**
     * Tag string to use in logging message.
     */
    private static final String CLASS_NAME = AppUtils.class.getSimpleName();

    /**
     * Private constructor
     */
    private AppUtils() {}

    /**
     * Read resource file as bytes array.
     *
     * @param id      Identifier of the resource.
     * @param context Application context.
     * @return Bytes array associated with a resource
     */
    public static byte[] getResource(final int id, final Context context) {
        final Resources resources = context.getResources();
        final InputStream is = resources.openRawResource(id);
        final ByteArrayOutputStream bout = new ByteArrayOutputStream();
        final byte[] readBuffer = new byte[4 * 1024];

        try {
            int read;
            do {
                read = is.read(readBuffer, 0, readBuffer.length);
                if(read == -1) {
                    break;
                }
                bout.write(readBuffer, 0, read);
            } while(true);

            return bout.toByteArray();
        } catch (IOException e) {
            /**/
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                /* Ignore */
            }
        }
        return new byte[0];
    }

    /**
     * Get application's version name.
     *
     * @param context Application context.
     * @return Application Version name.
     */
    public static String getApplicationVersion(final Context context) {
        final PackageInfo packageInfo = getPackageInfo(context);
        if (packageInfo != null) {
            return packageInfo.versionName;
        } else {
            AppLogger.w(CLASS_NAME + " Can't get application version");
            return "?";
        }
    }

    /**
     * Get application's version code.
     *
     * @param context Application context.
     * @return Application Version code.
     */
    public static int getApplicationCode(final Context context) {
        final PackageInfo packageInfo = getPackageInfo(context);
        if (packageInfo != null) {
            return packageInfo.versionCode;
        } else {
            AppLogger.w(CLASS_NAME + " Can't get code version");
            return 0;
        }
    }

    /**
     * @return PackageInfo for the current application or null if the PackageManager could not be
     * contacted.
     */
    private static PackageInfo getPackageInfo(final Context context) {
        final PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            AppLogger.w(CLASS_NAME + " Package manager is NULL");
            return null;
        }
        String packageName = "";
        try {
            packageName = context.getPackageName();
            return packageManager.getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            /**/
            return null;
        } catch (RuntimeException e) {
            // To catch RuntimeException("Package manager has died") that can occur on some
            // version of Android,
            // when the remote PackageManager is unavailable. I suspect this sometimes occurs
            // when the App is being reinstalled.
            /**/
            return null;
        } catch (Throwable e) {
            AppLogger.e(CLASS_NAME + " Package manager has Throwable : " + e);
            return null;
        }
    }

    /**
     * This is a helper method with allows to prevent get a list of the predefined categories,
     * in order to do not show an empty category.
     *
     * @return Collection of the categories.
     */
    public static Set<String> predefinedCategories() {
        final Set<String> predefinedCategories = new TreeSet<>();
        predefinedCategories.add("Classical");
        predefinedCategories.add("Country");
        predefinedCategories.add("Decades");
        predefinedCategories.add("Electronic");
        predefinedCategories.add("Folk");
        predefinedCategories.add("International");
        predefinedCategories.add("Jazz");
        predefinedCategories.add("Misc");
        predefinedCategories.add("Pop");
        predefinedCategories.add("R&B/Urban");
        predefinedCategories.add("Rap");
        predefinedCategories.add("Reggae");
        predefinedCategories.add("Rock");
        predefinedCategories.add("Talk & Speech");
        return predefinedCategories;
    }

    /**
     * This method addToLocals provided Bitmap to the specified file.
     *
     * @param bitmap   Bitmap data.
     * @param dirName  Path to the directory.
     * @param fileName Name of the file.
     */
    public static void saveBitmapToFile(final Bitmap bitmap, final String dirName,
                                        final String fileName) {

        if (bitmap == null) {
            AppLogger.e(CLASS_NAME + " Save bitmap to file, bitmap is null");
            return;
        }
        // Create directory if needed
        createDirIfNeeded(dirName);

        //create a file to write bitmap data
        final File file = new File(dirName + "/" + fileName);

        // http://stackoverflow.com/questions/11539657/open-failed-ebusy-device-or-resource-busy
        //noinspection ResultOfMethodCallIgnored
        file.renameTo(file);
        //noinspection ResultOfMethodCallIgnored
        file.delete();

        //Convert bitmap to byte array
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        final byte[] byteArray = byteArrayOutputStream.toByteArray();

        //write the bytes in file
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(byteArray);
        } catch (IOException e) {
            /**/
        } finally {
            try {
                byteArrayOutputStream.flush();
            } catch (IOException e) {
                /* Ignore */
            }
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                /* Ignore */
            }
        }
    }

    /**
     * Save data bytes to a file
     *
     * @param data     data as bytes array
     * @param filePath a path to file
     *
     * @return true in case of success, false - otherwise
     */
    private static boolean saveDataToFile(byte[] data, String filePath) {
        if (data == null) {
            AppLogger.w(CLASS_NAME + " Save data to file -> data is null, path:" + filePath);
            return false;
        }
        File file = new File(filePath);
        AppLogger.d(CLASS_NAME + " Saving data to file '" + filePath + "', exists:" + file.exists());
        if (file.exists()) {
            //noinspection ResultOfMethodCallIgnored
            file.delete();
        }
        FileOutputStream mFileOutputStream = null;
        boolean result = false;
        try {
            mFileOutputStream = new FileOutputStream(file.getPath());
            mFileOutputStream.write(data);

            result = true;
        } catch (IOException e) {
            /**/
        } finally {
            if (mFileOutputStream != null) {
                try {
                    mFileOutputStream.close();
                } catch (IOException e) {
                    /**/
                }
            }
        }
        return result;
    }

    /**
     *
     * @param data
     * @param dirName
     * @param fileName
     * @return
     */
    public static boolean saveDataToFile(final byte[] data, final String dirName,
                                         final String fileName) {
        createDirIfNeeded(dirName);
        return saveDataToFile(data, dirName + "/" + fileName);
    }

    /**
     * This method creates a directory with given name is such does not exists
     *
     * @param path a path to the directory
     */
    public static void createDirIfNeeded(final String path) {
        final File file = new File(path);
        if (file.exists() && !file.isDirectory()) {
            //noinspection ResultOfMethodCallIgnored
            file.delete();
        }
        if (!file.exists()) {
            //noinspection ResultOfMethodCallIgnored
            file.mkdirs();
        }
    }

    /**
     *
     * @param path
     * @return
     */
    public static File createFileIfNeeded(final String path) {
        final File file = new File(path);
        try {
            final boolean result = file.createNewFile();
        } catch (final IOException e) {
            /**/
        }
        return file;
    }

    public static boolean isFileExist(final String path) {
        final File file = new File(path);
        return file.exists() && !file.isDirectory();
    }

    /**
     * Return {@link File} object legal to call on API 8.
     *
     * @param type The type of files directory to return. May be null for the root of the
     *             files directory or one of the following Environment constants for a subdirectory:
     *             DIRECTORY_MUSIC, DIRECTORY_PODCASTS, DIRECTORY_RINGTONES, DIRECTORY_ALARMS,
     *             DIRECTORY_NOTIFICATIONS, DIRECTORY_PICTURES, or DIRECTORY_MOVIES.
     * @param context Context of the callee.
     * @return {@link File} object.
     */
    @TargetApi(8)
    private static File getExternalFilesDirAPI8(final Context context, final String type) {
        return context.getExternalFilesDir(type);
    }

    public static String getExternalStorageDir(final Context context) {
        final File externalDir = getExternalFilesDirAPI8(context, null);
        return externalDir != null ? externalDir.getAbsolutePath() : null;
    }

    public static int getLongestScreenSize(FragmentActivity context) {
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;
        return height > width ? height : width;
    }

    /**
     *
     * @param context
     * @return
     */
    public static int getShortestScreenSize(final Activity context) {
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;
        return height < width ? height : width;
    }

    public static boolean externalStorageAvailable() {
        boolean externalStorageAvailable;
        boolean externalStorageWriteable;
        final String state = Environment.getExternalStorageState();
        switch (state) {
            case Environment.MEDIA_MOUNTED:
                // We can read and write the media
                externalStorageAvailable = externalStorageWriteable = true;
                break;
            case Environment.MEDIA_MOUNTED_READ_ONLY:
                // We can only read the media
                externalStorageAvailable = true;
                externalStorageWriteable = false;
                break;
            default:
                // Something else is wrong. It may be one of many other states, but all we need
                //  to know is we can neither read nor write
                externalStorageAvailable = externalStorageWriteable = false;
                break;
        }
        return externalStorageAvailable && externalStorageWriteable;
    }

    public static String getApplicationVersionName(final Context context) {
        final PackageInfo packageInfo = getPackageInfo(context);
        if (packageInfo != null) {
            return packageInfo.versionName;
        } else {
            AppLogger.w("Can't get application version");
            return "?";
        }
    }

    public static int getApplicationVersionCode(final Context context) {
        final PackageInfo packageInfo = getPackageInfo(context);
        if (packageInfo != null) {
            return packageInfo.versionCode;
        } else {
            AppLogger.w("Can't get application code");
            return 0;
        }
    }

    /**
     * Get ISO 3166-1 alpha-2 country code for this device (or null if not available).
     *
     * @param context Context reference to get the TelephonyManager instance from.
     * @return country code or null
     */
    public static String getUserCountry(final Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(
                    Context.TELEPHONY_SERVICE
            );
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) {
                // SIM country code is available
                return simCountry.toLowerCase(Locale.US);
            } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) {
                // device is not 3G (would be unreliable)
                final String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) {
                    // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        } catch (final Exception e) {
            /**/
        }
        return null;
    }
}
