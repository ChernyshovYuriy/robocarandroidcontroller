package yurii.com.robocarandroid.api;

import android.text.TextUtils;

import yurii.com.robocarandroid.net.NetRequester;
import yurii.com.robocarandroid.net.UrlBuilder;
import yurii.com.robocarandroid.utils.AppLogger;

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 12/15/14
 * E-Mail: chernyshov.yuriy@gmail.com
 *
 */
public final class APIServiceProviderImpl implements APIServiceProvider {

    /**
     * Tag string to use in logging messages.
     */
    @SuppressWarnings("unused")
    private static final String CLASS_NAME = APIServiceProviderImpl.class.getSimpleName();

    private static final String CONNECTION_KEYWORD = "-- Robocar greeting you --";

    private NetRequester mRequester;

    /**
     * Constructor.
     *
     */
    public APIServiceProviderImpl(NetRequester requester) {
        super();
        mRequester = requester;
    }

    @Override
    public String getBatteryLevel() {
        // Get response from the server
        final String response = new String(mRequester.sendRequest(
                UrlBuilder.INSTANCE.getBatteryLevelUrl(), "GET")
        );
        return response;
    }

    @Override
    public Boolean getConStatus() {
        // Get response from the server
        final String response = new String(mRequester.sendRequest(
                UrlBuilder.INSTANCE.getEchoUrl(), "GET")
        );
        AppLogger.d("GetConStatus res:" + response);
        return TextUtils.equals(CONNECTION_KEYWORD, response);
    }

    @Override
    public void motorsStop() {
        final String response = new String(mRequester.sendRequest(
                UrlBuilder.INSTANCE.getMotorsStopUrl(), "POST")
        );
    }

    @Override
    public void motorsFwd() {
        final String response = new String(mRequester.sendRequest(
                UrlBuilder.INSTANCE.getMotorsFwdUrl(), "POST")
        );
    }

    @Override
    public void motorsBwd() {
        final String response = new String(mRequester.sendRequest(
                UrlBuilder.INSTANCE.getMotorsBwdUrl(), "POST")
        );
    }

    @Override
    public void motorsTurnRight() {
        final String response = new String(mRequester.sendRequest(
                UrlBuilder.INSTANCE.getMotorsTurnRightUrl(), "POST")
        );
    }

    @Override
    public void motorsTurnLeft() {
        final String response = new String(mRequester.sendRequest(
                UrlBuilder.INSTANCE.getMotorsTurnLeftUrl(), "POST")
        );
    }

    @Override
    public void autonomousStart() {
        final String response = new String(mRequester.sendRequest(
                UrlBuilder.INSTANCE.getAutonomousStartUrl(), "POST")
        );
    }

    @Override
    public void autonomousStop() {
        final String response = new String(mRequester.sendRequest(
                UrlBuilder.INSTANCE.getAutonomousStopUrl(), "POST")
        );
    }
}
