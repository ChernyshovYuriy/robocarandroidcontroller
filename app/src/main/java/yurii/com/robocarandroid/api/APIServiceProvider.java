package yurii.com.robocarandroid.api;

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 12/14/14
 * E-Mail: chernyshov.yuriy@gmail.com
 */

/**
 * {@link APIServiceProvider} is an interface which provide
 * various methods of the Dirble's API.
 */
public interface APIServiceProvider {

    String getBatteryLevel();

    Boolean getConStatus();

    void motorsStop();

    void motorsFwd();

    void motorsBwd();

    void motorsTurnRight();

    void motorsTurnLeft();

    void autonomousStart();

    void autonomousStop();
}
