package yurii.com.robocarandroid.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 22/12/18
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public final class PagerAdapterExt extends FragmentStatePagerAdapter {

    private final int mNumOfTabs;

    PagerAdapterExt(final FragmentManager fragmentManager, final int numOfTabs) {
        super(fragmentManager);
        mNumOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ManualControlFragment();
            case 1:
                return new AutonomousControlFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
