package yurii.com.robocarandroid.ui;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import yurii.com.robocarandroid.net.UrlBuilder;
import yurii.com.robocarandroid.persist.PersistData;
import yurii.com.robocarandroid.utils.AppLogger;

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 21/12/18
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public final class SettingsDialogPresenterImpl implements SettingDialogPresenter {

    private final PersistData mPersistData;

    public SettingsDialogPresenterImpl(@NonNull final PersistData persistData) {
        super();
        mPersistData = persistData;
    }

    @Override
    public void savePiIpAddress(@Nullable final String value) {
        if (TextUtils.isEmpty(value)) {
            AppLogger.e("Can not handle invalid Pi IP Address:" + value);
            return;
        }
        mPersistData.savePiIpAddress(value);
        UrlBuilder.INSTANCE.updateBaseUrl(value);
    }

    @Override
    public void savePiLogin(final String value) {
        if (TextUtils.isEmpty(value)) {
            AppLogger.e("Can not handle invalid Pi Lgn:" + value);
            return;
        }
        mPersistData.savePiLogin(value);
    }

    @Override
    public void savePiPwd(final String value) {
        if (TextUtils.isEmpty(value)) {
            AppLogger.e("Can not handle invalid Pi Pwd:" + value);
            return;
        }
        mPersistData.savePiPwd(value);
    }

    @Override
    public String loadPiIpAddress() {
        final String value = mPersistData.loadPiIpAddress();
        return value;
    }

    @Override
    public String loadPiLogin() {
        final String value = mPersistData.loadPiLogin();
        return value;
    }

    @Override
    public String loadPiPwd() {
        final String value = mPersistData.loadPiPwd();
        return value;
    }
}
