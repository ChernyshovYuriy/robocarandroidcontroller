package yurii.com.robocarandroid.ui;

import android.os.Handler;
import android.support.annotation.NonNull;

import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import yurii.com.robocarandroid.api.APIServiceProvider;
import yurii.com.robocarandroid.net.PiConnector;
import yurii.com.robocarandroid.utils.AppLogger;

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 21/12/18
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public class MainActivityPresenterImpl implements MainActivityPresenter {

    private static final int MONITORING_PERIOD = 3000;
    private final Handler mHandler;
    private final Runnable mRunnable;
    private final ExecutorService mExecutorService;
    private volatile boolean mIsMonitoring;
    private volatile Boolean mIsConnected;
    private final APIServiceProvider mApiServiceProvider;
    private Listener mListener;
    private PiConnector mPiConnector;

    public MainActivityPresenterImpl(@NonNull final Handler handler,
                                     @NonNull final ExecutorService executorService,
                                     @NonNull final APIServiceProvider apiServiceProvider,
                                     @NonNull final PiConnector piConnector,
                                     @NonNull final Listener listener) {
        super();
        mHandler = handler;
        mExecutorService = executorService;
        mApiServiceProvider = apiServiceProvider;
        mRunnable = new RunnableImpl(this);
        mIsMonitoring = false;
        mIsConnected = false;
        mListener = listener;
        mPiConnector = piConnector;
    }

    @Override
    public void doPiConnect() {
        mPiConnector.connect();
    }

    @Override
    public void startMonitoring() {
        if (mIsMonitoring) {
            return;
        }
        mIsMonitoring = true;
        mHandler.post(mRunnable);
    }

    @Override
    public void stopMonitoring() {
        if (!mIsMonitoring) {
            return;
        }
        mIsMonitoring = false;
        mHandler.removeCallbacks(mRunnable);
    }

    @Override
    public void motorsStop() {
        mExecutorService.submit(mApiServiceProvider::motorsStop);
    }

    @Override
    public void motorsFwd() {
        mExecutorService.submit(mApiServiceProvider::motorsFwd);
    }

    @Override
    public void motorsBwd() {
        mExecutorService.submit(mApiServiceProvider::motorsBwd);
    }

    @Override
    public void motorsTurnRight() {
        mExecutorService.submit(mApiServiceProvider::motorsTurnRight);
    }

    @Override
    public void motorsTurnLeft() {
        mExecutorService.submit(mApiServiceProvider::motorsTurnLeft);
    }

    @Override
    public void autonomousStart() {
        mExecutorService.submit(mApiServiceProvider::autonomousStart);
    }

    @Override
    public void autonomousStop() {
        mExecutorService.submit(mApiServiceProvider::autonomousStop);
    }

    private void onMonitorEvent() {
        AppLogger.i("On main monitoring");
        mHandler.postDelayed(mRunnable, MONITORING_PERIOD);

        boolean isConnected = false;
        final Future<Boolean> result = mExecutorService.submit(mApiServiceProvider::getConStatus);
        try {
            isConnected = result.get();
        } catch (final ExecutionException e) {
            /**/
        } catch (final InterruptedException e) {
            /**/
        }
        if (mIsConnected != isConnected) {
            mListener.onConnectionChanged(isConnected);
        }
        mIsConnected = isConnected;
    }

    private static class RunnableImpl implements Runnable {

        private final WeakReference<MainActivityPresenterImpl> mReference;

        private RunnableImpl(final MainActivityPresenterImpl reference) {
            super();
            mReference = new WeakReference<>(reference);
        }

        @Override
        public void run() {
            final MainActivityPresenterImpl reference = mReference.get();
            if (reference == null) {
                return;
            }
            reference.onMonitorEvent();
        }
    }
}
