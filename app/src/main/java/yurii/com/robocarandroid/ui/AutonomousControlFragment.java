package yurii.com.robocarandroid.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import yurii.com.robocarandroid.R;

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 22/12/18
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public class AutonomousControlFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ctrl_autonomous, container, false);
    }
}
