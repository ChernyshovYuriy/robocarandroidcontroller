package yurii.com.robocarandroid.ui;

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 21/12/18
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public interface MainActivityPresenter {

    interface Listener {

        void onConnectionChanged(final boolean isConnected);
    }

    void doPiConnect();

    void startMonitoring();

    void stopMonitoring();

    void motorsStop();

    void motorsFwd();

    void motorsBwd();

    void motorsTurnRight();

    void motorsTurnLeft();

    void autonomousStart();

    void autonomousStop();
}
