package yurii.com.robocarandroid.ui;

import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ToggleButton;

import java.lang.ref.WeakReference;

import yurii.com.robocarandroid.R;
import yurii.com.robocarandroid.dependencies.DependencyRegistry;
import yurii.com.robocarandroid.utils.AppLogger;

public final class MainActivity extends AppCompatActivity {

    private MainActivityPresenter mPresenter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ViewPager viewPager = findViewById(R.id.ctrl_view_pager);
        final TabLayout tabLayout = findViewById(R.id.ctrl_tab_layout);

        final PagerAdapter adapter = new PagerAdapterExt(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(
                new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        AppLogger.i("Ctrl tab selected:" + tab.getText());
                        viewPager.setCurrentItem(tab.getPosition());
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        AppLogger.i("Ctrl tab unselected:" + tab.getText());
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        AppLogger.i("Ctrl tab reselected:" + tab.getText());
                    }
                }
        );

        DependencyRegistry.INSTANCE.inject(this, savedInstanceState, new ListenerImpl(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPresenter != null) {
            mPresenter.doPiConnect();
            mPresenter.startMonitoring();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mPresenter != null) {
            mPresenter.stopMonitoring();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        DependencyRegistry.INSTANCE.remove(this);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        final int id = item.getItemId();

        if (id == R.id.action_settings) {
            final FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.addToBackStack(null);
            final DialogFragment settingsDialog = SettingsDialog.newInstance();
            settingsDialog.show(fragmentTransaction, SettingsDialog.DIALOG_TAG);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void configureWith(@NonNull final MainActivityPresenter presenter) {
        mPresenter = presenter;
        mPresenter.startMonitoring();
    }

    public void handleMotorsStopClick(final View view) {
        if (mPresenter != null) {
            mPresenter.motorsStop();
        }
    }

    public void handleMotorsFwdClick(final View view) {
        if (mPresenter != null) {
            mPresenter.motorsFwd();
        }
    }

    public void handleMotorsBwdClick(final View view) {
        if (mPresenter != null) {
            mPresenter.motorsBwd();
        }
    }

    public void handleMotorsTurnLeftClick(final View view) {
        if (mPresenter != null) {
            mPresenter.motorsTurnLeft();
        }
    }

    public void handleMotorsTurnRightClick(final View view) {
        if (mPresenter != null) {
            mPresenter.motorsTurnRight();
        }
    }

    public void handleAutonomousStartClick(final View view) {
        if (mPresenter != null) {
            mPresenter.autonomousStart();
        }
    }

    public void handleAutonomousStopClick(final View view) {
        if (mPresenter != null) {
            mPresenter.autonomousStop();
        }
    }

    private void handleConnectionChanged(final boolean isConnected) {
        final ToggleButton button = findViewById(R.id.strl_con_status);
        button.setChecked(isConnected);
    }

    private static class ListenerImpl implements MainActivityPresenter.Listener {

        private final WeakReference<MainActivity> mReference;

        private ListenerImpl(final MainActivity reference) {
            super();
            mReference = new WeakReference<>(reference);
        }

        @Override
        public void onConnectionChanged(final boolean isConnected) {
            final MainActivity reference = mReference.get();
            if (reference == null) {
                return;
            }
            reference.runOnUiThread(() -> reference.handleConnectionChanged(isConnected));
        }
    }
}
