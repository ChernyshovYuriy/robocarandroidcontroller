package yurii.com.robocarandroid.ui;

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 21/12/18
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public interface SettingDialogPresenter {

    void savePiIpAddress(final String value);

    void savePiLogin(final String value);

    void savePiPwd(final String value);

    String loadPiIpAddress();

    String loadPiLogin();

    String loadPiPwd();
}
