package yurii.com.robocarandroid.ui;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import yurii.com.robocarandroid.R;
import yurii.com.robocarandroid.dependencies.DependencyRegistry;

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 12/20/14
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public final class SettingsDialog extends DialogFragment {

    /**
     * Tag string mTo use in logging message.
     */
    private static final String CLASS_NAME = SettingsDialog.class.getSimpleName();

    /**
     * Tag string mTo use in dialog transactions.
     */
    public static final String DIALOG_TAG = CLASS_NAME + "_DIALOG_TAG";

    private SettingDialogPresenter mPresenter;

    /**
     * Create a new instance of {@link SettingsDialog}
     */
    @SuppressWarnings("all")
    public static SettingsDialog newInstance() {
        final SettingsDialog aboutDialog = new SettingsDialog();
        // provide here an arguments, if any
        return aboutDialog;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.settings_dialog, container, false);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DependencyRegistry.INSTANCE.inject(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mPresenter == null) {
            return;
        }

        final EditText piIpEdit = getView().findViewById(R.id.pi_ip_edit);
        final EditText piLgnEdit = getView().findViewById(R.id.pi_lgn_edit);
        final EditText piPwdEdit = getView().findViewById(R.id.pi_pwd_edit);

        mPresenter.savePiIpAddress(piIpEdit.getText().toString().trim());
        mPresenter.savePiLogin(piLgnEdit.getText().toString().trim());
        mPresenter.savePiPwd(piPwdEdit.getText().toString().trim());
    }

    public void configureWith(@NonNull final SettingDialogPresenter presenter) {
        mPresenter = presenter;

        final EditText piIpEdit = getView().findViewById(R.id.pi_ip_edit);
        final EditText piLgnEdit = getView().findViewById(R.id.pi_lgn_edit);
        final EditText piPwdEdit = getView().findViewById(R.id.pi_pwd_edit);

        piIpEdit.setText(mPresenter.loadPiIpAddress());
        piLgnEdit.setText(mPresenter.loadPiLogin());
        piPwdEdit.setText(mPresenter.loadPiPwd());
    }
}
