package yurii.com.robocarandroid.net;

import android.net.Uri;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import yurii.com.robocarandroid.utils.AppLogger;

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 21/12/18
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public class HttpNetRequester implements NetRequester {

    /**
     * Tag to use in logging message.
     */
    @SuppressWarnings("unused")
    private static final String CLASS_NAME = HttpNetRequester.class.getSimpleName();

    /**
     * The default buffer size ({@value}) to use for
     * {@link #copyLarge(InputStream, OutputStream)}
     */
    private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;

    /**
     * Represents the end-of-file (or stream).
     */
    private static final int EOF = -1;

    public HttpNetRequester() {
        super();
    }

    @Override
    public byte[] sendRequest(final Uri uri, final String method) {
        AppLogger.i(CLASS_NAME + " Request URL:" + uri);
        byte[] response = new byte[0];

        URL url = null;
        try {
            url = new URL(uri.toString());
        } catch (final MalformedURLException exception) {
//            FabricUtils.logException(
//                    new DownloaderException(
//                            createExceptionMessage(uri, parameters),
//                            exception
//                    )
//            );
        }

        if (url == null) {
            return response;
        }

        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
        } catch (final IOException exception) {
//            FabricUtils.logException(
//                    new DownloaderException(
//                            createExceptionMessage(uri, parameters),
//                            exception
//                    )
//            );
        }

        if (urlConnection == null) {
            return response;
        }

        int responseCode = 0;
        try {
            urlConnection.setRequestMethod(method);
            responseCode = urlConnection.getResponseCode();
        } catch (final IOException exception) {
//            FabricUtils.logException(
//                    new DownloaderException(
//                            createExceptionMessage(uri, parameters),
//                            exception
//                    )
//            );
        }

        AppLogger.d("Response code:" + responseCode);
        if (responseCode < 200 || responseCode > 299) {
            urlConnection.disconnect();
            return response;
        }

        try {
            final InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
            response = toByteArray(inputStream);
        } catch (final IOException exception) {
//            FabricUtils.logException(
//                    new DownloaderException(
//                            createExceptionMessage(uri, parameters),
//                            exception
//                    )
//            );
        } finally {
            urlConnection.disconnect();
        }

        return response;
    }

    /**
     * Gets the contents of an <code>InputStream</code> as a <code>byte[]</code>.
     * <p>
     * This method buffers the input internally, so there is no need to use a
     * <code>BufferedInputStream</code>.
     *
     * @param input the <code>InputStream</code> to read from
     * @return the requested byte array
     * @throws NullPointerException if the input is null
     * @throws IOException          if an I/O error occurs
     */
    private static byte[] toByteArray(final InputStream input) throws IOException {
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        copy(input, output);
        return output.toByteArray();
    }

    /**
     * Copies bytes from an <code>InputStream</code> to an
     * <code>OutputStream</code>.
     * <p>
     * This method buffers the input internally, so there is no need to use a
     * <code>BufferedInputStream</code>.
     * <p>
     * Large streams (over 2GB) will return a bytes copied value of
     * <code>-1</code> after the copy has completed since the correct
     * number of bytes cannot be returned as an int. For large streams
     * use the <code>copyLarge(InputStream, OutputStream)</code> method.
     *
     * @param input the <code>InputStream</code> to read from
     * @param output the <code>OutputStream</code> to write to
     * @return the number of bytes copied, or -1 if &gt; Integer.MAX_VALUE
     * @throws NullPointerException if the input or output is null
     * @throws IOException          if an I/O error occurs
     * @since 1.1
     */
    private static int copy(final InputStream input, final OutputStream output) throws IOException {
        final long count = copyLarge(input, output);
        if (count > Integer.MAX_VALUE) {
            return -1;
        }
        return (int) count;
    }

    /**
     * Copies bytes from a large (over 2GB) <code>InputStream</code> to an
     * <code>OutputStream</code>.
     * <p>
     * This method buffers the input internally, so there is no need to use a
     * <code>BufferedInputStream</code>.
     * <p>
     * The buffer size is given by {@link #DEFAULT_BUFFER_SIZE}.
     *
     * @param input the <code>InputStream</code> to read from
     * @param output the <code>OutputStream</code> to write to
     * @return the number of bytes copied
     * @throws NullPointerException if the input or output is null
     * @throws IOException          if an I/O error occurs
     * @since 1.3
     */
    private static long copyLarge(final InputStream input, final OutputStream output)
            throws IOException {
        return copy(input, output, DEFAULT_BUFFER_SIZE);
    }

    /**
     * Copies bytes from an <code>InputStream</code> to an <code>OutputStream</code>
     * using an internal buffer of the given size.
     * <p>
     * This method buffers the input internally, so there is no need to use
     * a <code>BufferedInputStream</code>.
     * <p>
     *
     * @param input the <code>InputStream</code> to read from
     * @param output the <code>OutputStream</code> to write to
     * @param bufferSize the bufferSize used to copy from the input to the output
     * @return the number of bytes copied
     * @throws NullPointerException if the input or output is null
     * @throws IOException          if an I/O error occurs
     * @since 2.5
     */
    public static long copy(final InputStream input, final OutputStream output,
                            final int bufferSize)
            throws IOException {
        return copyLarge(input, output, new byte[bufferSize]);
    }

    /**
     * Copies bytes from a large (over 2GB) <code>InputStream</code> to an
     * <code>OutputStream</code>.
     * <p>
     * This method uses the provided buffer, so there is no need to use a
     * <code>BufferedInputStream</code>.
     * <p>
     *
     * @param input the <code>InputStream</code> to read from
     * @param output the <code>OutputStream</code> to write to
     * @param buffer the buffer to use for the copy
     * @return the number of bytes copied
     * @throws NullPointerException if the input or output is null
     * @throws IOException          if an I/O error occurs
     * @since 2.2
     */
    private static long copyLarge(final InputStream input, final OutputStream output,
                                  final byte[] buffer)
            throws IOException {
        long count = 0;
        int n;
        while (EOF != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }
}
