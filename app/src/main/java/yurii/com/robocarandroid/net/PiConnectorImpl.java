package yurii.com.robocarandroid.net;

import android.support.annotation.NonNull;
import android.util.Log;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.ByteArrayOutputStream;
import java.util.Properties;
import java.util.concurrent.ExecutorService;

import yurii.com.robocarandroid.persist.PersistData;
import yurii.com.robocarandroid.utils.AppLogger;

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 22/12/18
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public final class PiConnectorImpl implements PiConnector {

    private final ExecutorService mExecutorService;
    private PersistData mPersistData;

    public PiConnectorImpl(@NonNull final ExecutorService executorService,
                           @NonNull final PersistData persistData) {
        super();
        mExecutorService = executorService;
        mPersistData = persistData;
    }

    @Override
    public void connect() {
        mExecutorService.submit(
                () -> {
                    try {
                        executeConnection(
                                mPersistData.loadPiLogin(),
                                mPersistData.loadPiPwd(),
                                mPersistData.loadPiIpAddress(),
                                22
                        );
                    } catch (final Exception e) {
                        AppLogger.e("Can not execute Pi Cmd:" + Log.getStackTraceString(e));
                    }
                }
        );
    }

    private String executeConnection(@NonNull final String username,
                                     @NonNull final String password,
                                     @NonNull final String hostname,
                                     final int port) throws Exception {
        final JSch jsch = new JSch();
        final Session session = jsch.getSession(username, hostname, port);
        session.setPassword(password);

        // Avoid asking for key confirmation
        final Properties prop = new Properties();
        prop.put("StrictHostKeyChecking", "no");
        session.setConfig(prop);

        session.connect();

        // SSH Channel
        final ChannelExec channelssh = (ChannelExec) session.openChannel("exec");
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        channelssh.setErrStream(baos);
        channelssh.setOutputStream(baos);
        channelssh.setExtOutputStream(baos);

        // Execute command
        channelssh.setCommand("cd ~/dev/robocar");
        channelssh.connect();
        channelssh.disconnect();

//        channelssh.setCommand("git pull");
//        channelssh.connect();
//        channelssh.disconnect();

        channelssh.setCommand("python3 py/main.py");
        channelssh.connect();
        channelssh.disconnect();

        final String result = new String(baos.toByteArray());

        AppLogger.i("PiCmd result:" + result);

        return result;
    }
}
