package yurii.com.robocarandroid.net;

import android.net.Uri;

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 21/12/18
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public interface NetRequester {

    byte[] sendRequest(final Uri uri, String method);
}
