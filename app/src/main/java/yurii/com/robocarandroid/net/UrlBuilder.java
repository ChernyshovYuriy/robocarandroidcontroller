package yurii.com.robocarandroid.net;

import android.net.Uri;

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 12/15/14
 * E-Mail: chernyshov.yuriy@gmail.com
 *
 * {@link UrlBuilder} is a helper class which performs
 * build URL of the different API calls.
 */
public enum UrlBuilder {
    
    INSTANCE;

    /**
     * Base URL for the API requests.
     */
    private String mBaseUrl;

    /**
     * Private constructor.
     */
    UrlBuilder() {}

    public void updateBaseUrl(final String value) {
        //TODO: Validate
        mBaseUrl = "http://" + value + ":8080/";
    }

    /**
     * Get Uri for "Battery Level" check call.
     *
     * @return {@link Uri}.
     */
    public Uri getBatteryLevelUrl() {
        return Uri.parse(mBaseUrl + "battery/level");
    }

    /**
     * Get Uri for "Echo" check call.
     *
     * @return {@link Uri}.
     */
    public Uri getEchoUrl() {
        return Uri.parse(mBaseUrl + "echo");
    }

    public Uri getMotorsStopUrl() {
        return Uri.parse(mBaseUrl + "motors/stop");
    }

    public Uri getMotorsFwdUrl() {
        return Uri.parse(mBaseUrl + "motors/fwd");
    }

    public Uri getMotorsBwdUrl() {
        return Uri.parse(mBaseUrl + "motors/bwd");
    }

    public Uri getMotorsTurnLeftUrl() {
        return Uri.parse(mBaseUrl + "motors/turn/left");
    }

    public Uri getMotorsTurnRightUrl() {
        return Uri.parse(mBaseUrl + "motors/turn/right");
    }

    public Uri getAutonomousStartUrl() {
        return Uri.parse(mBaseUrl + "start");
    }

    public Uri getAutonomousStopUrl() {
        return Uri.parse(mBaseUrl + "stop");
    }
}
