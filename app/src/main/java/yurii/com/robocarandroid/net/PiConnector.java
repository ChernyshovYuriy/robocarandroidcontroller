package yurii.com.robocarandroid.net;

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 22/12/18
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public interface PiConnector {

    void connect();
}
