package yurii.com.robocarandroid;

import android.app.Application;
import android.os.Build;

import yurii.com.robocarandroid.dependencies.DependencyRegistry;
import yurii.com.robocarandroid.utils.AppLogger;
import yurii.com.robocarandroid.utils.AppUtils;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 12/21/13
 * Time: 6:29 PM
 */
public final class MainApp extends Application {

    /**
     * Constructor.
     */
    public MainApp() {
        super();
    }

    @Override
    public final void onCreate() {
        super.onCreate();
        DependencyRegistry.INSTANCE.init(getApplicationContext());
    }

    /**
     * Print first log message with summary information about device and application.
     */
    @SuppressWarnings("all")
    private void printFirstLogMessage() {
        final StringBuilder firstLogMessage = new StringBuilder();
        firstLogMessage.append("\n");
        firstLogMessage.append("########### Create '");
        firstLogMessage.append(getString(R.string.app_name));
        firstLogMessage.append("' Application ###########\n");
        firstLogMessage.append("- processors: ");
        firstLogMessage.append(Runtime.getRuntime().availableProcessors());
        firstLogMessage.append("\n");
        firstLogMessage.append("- version: ");
        firstLogMessage.append(AppUtils.getApplicationVersionCode(this));
        firstLogMessage.append(".");
        firstLogMessage.append(AppUtils.getApplicationVersionName(this));
        firstLogMessage.append("\n");
        firstLogMessage.append("- OS ver: ");
        firstLogMessage.append(Build.VERSION.RELEASE);
        firstLogMessage.append("\n");
        firstLogMessage.append("- API level: ");
        firstLogMessage.append(Build.VERSION.SDK_INT);
        firstLogMessage.append("\n");
        firstLogMessage.append("- Country: ");
        firstLogMessage.append(AppUtils.getUserCountry(this));

        AppLogger.i(firstLogMessage.toString());
    }
}
