package yurii.com.robocarandroid.persist;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import yurii.com.robocarandroid.utils.AppLogger;

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 21/12/18
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public final class PersistDataImpl implements PersistData {

    private static final String PI_IP_ADDRESS_KEY = "PI_IP_ADDRESS_KEY";
    private static final String PI_LGN_KEY = "PI_LGN_KEY";
    private static final String PI_PWD_KEY = "PI_PWD_KEY";

    private final SharedPreferences mPreferences;

    public PersistDataImpl(@NonNull final SharedPreferences preferences) {
        super();
        mPreferences = preferences;
    }

    @Override
    public void savePiIpAddress(final String value) {
        mPreferences.edit().putString(PI_IP_ADDRESS_KEY, value).apply();
        AppLogger.i("Pi ip " + value + " saved");
    }

    @Override
    public void savePiLogin(final String value) {
        mPreferences.edit().putString(PI_LGN_KEY, value).apply();
        AppLogger.i("Pi lgn " + value + " saved");
    }

    @Override
    public void savePiPwd(final String value) {
        mPreferences.edit().putString(PI_PWD_KEY, value).apply();
        AppLogger.i("Pi pwd " + value + " saved");
    }

    @Override
    public String loadPiIpAddress() {
        final String value = mPreferences.getString(PI_IP_ADDRESS_KEY, "");
        AppLogger.i("Pi ip " + value + " loaded");
        return value;
    }

    @Override
    public String loadPiLogin() {
        final String value = mPreferences.getString(PI_LGN_KEY, "");
        AppLogger.i("Pi lgn " + value + " loaded");
        return value;
    }

    @Override
    public String loadPiPwd() {
        final String value = mPreferences.getString(PI_PWD_KEY, "");
        AppLogger.i("Pi pwd " + value + " loaded");
        return value;
    }
}
