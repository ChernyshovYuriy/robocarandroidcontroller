package yurii.com.robocarandroid.persist;

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 21/12/18
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public interface PersistData {

    void savePiIpAddress(final String value);

    void savePiLogin(final String value);

    void savePiPwd(final String value);

    String loadPiLogin();

    String loadPiPwd();

    String loadPiIpAddress();
}
